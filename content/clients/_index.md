+++
title = "Clients"
date = 2019-11-07T16:34:58+01:00
weight = 10
chapter = true
pre = "<b>1. </b>"
+++

# How to integrate with the Arbalo platform

After [registration](/clients/registration) clients have access to the Arbalo [service catalog](https://app.test.arbalo.dev/service/catalog) where they are free to provision and use services on a transaction basis.

By using the Arbalo platform clients are able to benefit from:

* Rapid provisioning of services
  * Quickly research, select, test and deploy an API to use
* Single Sign On
  * Use the same details to connect to all services
* One bill
  * Usage and billing is calculated by project with a breakdown available by client and by service
* Fair billing
  * Bills are calculated by usage. If you don't use the service you shouldn't have to pay for it.
* Transparency
  * Bring back visibility to the company infrastructure. Who's using what and how often. 

Register today and use this documentation to get started with Arbalo.

Problems? Contact us and help us improve our documentation.
