---
title: "Client Authentication"
date: 2019-11-14T21:47:43+01:00
draft: false

weight: 35
---

## Overview

This section describes the authentication of a client application that uses services on the Arbalo platform. This is typically a business logic tier running on your server. 

But there is also the possibility that a client (e.g. native mobile client or JavaScript Single-Page-App) may authenticate directly. We do not recommend this approach because the credentials can not be saved securely enough on the client.

There are different authentication mechanisms to deal with:

1. **Arbalo user authentication**: Arbalo's keycloak identity management system administers and grants user access to a configured projects on the Arbalo platform via OAuth2. Typical users are your project managers, sys admins, devops engineers or developers. Read more on [Registration and Login](/clients/registration).
2. Arbalo **client authentication** via BasicAuth and the client credentials described above. This is how your application is getting access to the services running on the Arbalo platform. It happens behind the scenes without any user interaction. This is covered below.
3. Your **application's user authentication**: normally, your application also needs to administer and authenticate the users that request access to your application. This authentication is application-specific and not covered by this documentation.

## Precondition

In order to use Arbalo services, you need to be [registered](/clients/registration).

## Configure the security access

The access to all services on the Arbalo platform needs to be secured, so that only you (or the people authorized for your Arbalo project) can access them. Your application needs to authenticate itself with valid client credentials over HTTP BasicAuth.

Therefore, you need to configure client credentials.

These client credentials are unique for all services used within your Arbalo project.

![Client Credentials](clientCredentials.png)

To do so, copy the Client ID and Client Secret from the settings in your Arbalo dashboard and configure them in your application.

> BEWARE: client credentials are sensitive data. Please keep them confidential and avoid that somebody else use services under your name. This also includes how you store the client credentials in your application. Don't check the credentials into a publicly accessible source code management system. Additionally, you should not use the credential within your own client application (e.g a JavaScript single-page app). Configure and use the client credentials within the business service layer of your application.

## Basic Auth

HTTP [Basic Auth](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#Basic_authentication_scheme) (BA) implementation is the simplest technique for enforcing access controls to web resources because it does not require cookies, session identifiers, or login pages; rather, HTTP Basic authentication uses standard fields in the HTTP header.

The BA mechanism provides no confidentiality protection for the transmitted credentials. They are merely encoded with Base64 in transit, but not encrypted or hashed in any way. Therefore, Arbalo enforces HTTPS to provide confidentiality.

To unauthenticated requests, Arbalo returns a response whose header contains a `HTTP 401 Unauthorized` status.

When your user agent wants to send authentication credentials to the Arbalo server, it may use the Authorization field.

The Authorization field is constructed as follows:

* The Client ID and Client Secret are combined with a single colon (:). This means that the Client ID itself cannot contain a colon.
* The resulting string is encoded into an octet sequence. The character set to use for this encoding is by default unspecified, as long as it is compatible with US-ASCII, but the server may suggest use of UTF-8 by sending the charset parameter.
* The resulting string is encoded using a variant of Base64.
* The authorization method and a space (e.g. "Basic ") is then prepended to the encoded string.

Example authorization header (with encoded Client ID and Client Secret):

`Authorization: Basic QEasdfZOKjqpkHRLkfh`

If the authentication was completed successfully, Arbalo replies with an access token and a refresh token. Your application may use the access token to cache the client credentials. The access token has to be delivered with each request to Arbalo.

HTTP does not provide a method for a web server (Arbalo in this case) to instruct the client (your application in this case) to 'log out'. That is why the access token has a rather short validity of some few minutes.

After the validity period of the access token is over, Arbalo sends again a `HTTP 401 Unauthorized` error. Your application should then re-authenticate with the client credentials or the refresh token to get another valid access token.

If you safely store the client credentials on the server side, you can ignore the refresh token and use the client credentials instead.

### Example: Get access token

#### Prerequisites

* Client ID and Client Secret from project dashboard
* `curl` or [`httpie`](https://httpie.org/) command line tools

**Note:** Replace the `{{ client ID }}` and `{{ client secret }}` placeholder values with the client ID and client secret respectively

```bash
# Using curl:
curl -X POST --silent --data 'grant_type=client_credentials' --user "{{ client ID }}:{{ client secret }}" "https://idp.arbalo.dev/auth/realms/arbalo.test/protocol/openid-connect/token"

# Using httpie
http --form --auth "{{ client ID }}:{{ client secret }}" POST "https://idp.arbalo.dev/auth/realms/arbalo.test/protocol/openid-connect/token" grant_type="client_credentials"
```
