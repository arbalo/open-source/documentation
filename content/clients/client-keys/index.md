---
title: "Client Keys"
date: 2019-11-12T07:45:39+01:00
draft: false

weight: 30
---

Arbalo uses a Single Sign On method of authentication removing the necessity to have one set of credentials per service.  Instead, users in a project manage client ID and secret pairs that are uses to [retrieve access tokens](/clients/authentication/#example-get-access-token).

From the dashboard of a project, go to "Settings" and select "Client Keys".

![Empty client credentials](no-client-credentials.png)

## Creating Client Keys

Using the green `+` (plus) button, a Client ID and Client Secret will be created with permissions to access all service instances provisioned in the project.

Client IDs will start with the project name to help with identification, followed by a random identifier.

## Removing/Disabling Client Keys

> Note: Client keys are never deleted, only disabled. This must be done for usage calculation based on the client ID

With the selected client key, pressing the red `X` (cross) will open a popup to confirm the removal of the client ID. **This will disable all access using this client key.**

![Disable client keys](disable-client-key.png)
